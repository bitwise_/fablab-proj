import logging
from random import randint
import tflite_runtime.interpreter as tflite
import numpy as np
import os

DEBUG_PREDICTIONS = ['1', '2', '3', '4',
                     'true', 'false', 'play', 'trivia', 'empty']
DEBUG_PREDICTIONS = [ pred for pred in DEBUG_PREDICTIONS for i in range(3) ]
DEBUG_PREDICTIONS.append('stop')

DEBUG = os.getenv('GAME_DEBUG', False)

log = logging.getLogger(__name__)

class Predictions():

    _STATE2TS = {
        'WELCOME': 'play_stop',
        'CHOOSE': 'games',
        'TRIVIA': 'play_stop',
        'MORRA': 'play_stop',
        'TRIS': 'play_stop',
        'EXIT': 'play_stop',
        'QUESTION': 'answers', 
        'EVAL': 'answers', 
        'CORRECT': 'play_stop', 
        'INCORRECT': 'play_stop', 
        'GAME_OVER': 'play_stop'
    }

    def __init__(self, camera):
        self.c = camera
        self.interp = None
        self.loaded_state = None
        self.ts = 'play_stop'
        self.sign = None
        self.data = np.ndarray(shape=(1, 224, 224, 3), dtype=np.float32)
        
    def _load_training_set(self, state):
        if self.loaded_state == state:
            log.debug('Training set gia caricato: {}'.format(self.loaded_state))
            return
        self.loaded_state = state
        self.ts = self._STATE2TS.get(self.loaded_state)
        # carica il training set specifico
        log.debug('Caricamento training set: {}'.format(self.ts))
        self.interp = tflite.Interpreter("training_sets/{}/model_unquant.tflite".format(self.ts))
        self.sign = self.interp.get_signature_runner('serving_default')

    def _prediction2label(self, prediction):
        key = None
        for e in list(prediction.keys()):
            if e.startswith('sequential'):
                key = e
                break
        max_prediction = max(prediction[key][0])
        log.debug('Predizione rilevata: {}'.format(max_prediction))
        label_index = list(prediction[key][0]).index(max_prediction)
        log.debug(f'Classe rilevata {label_index}')
        this_path = os.path.dirname(__file__)
        labels_path = os.path.join(this_path, '../training_sets/{}/labels.txt'.format(self.ts))
        with open(labels_path, 'r') as labels:
            all_labels = labels.read().split('\n')
            label = all_labels[label_index]
        return label[2:]
    
    def predict(self, state):
        if(DEBUG == 'true'):
            return DEBUG_PREDICTIONS[randint(0, len(DEBUG_PREDICTIONS) - 1)]
        log.debug('Predizione basata su trainig set per il riconoscimento delle azioni per lo stato: {}'.format(state))
        self._load_training_set(state)
        image = self.c.capture()
        image_array = np.asarray(image)
        normalized_image_array = (image_array.astype(np.float32) / 127.0) - 1
        self.data[0] = normalized_image_array


        sequential_raw = self.interp.get_input_details()[0]['name'].split('_')
        seq_in = {
            f"{sequential_raw[2]}_{sequential_raw[3]}_{sequential_raw[4][:-2]}": self.data
        }

        prediction = self.sign(**seq_in)
        
        return self._prediction2label(prediction)
    







