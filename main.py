import asyncio
import logging

from numpy import log

from core.Camera import Camera
from core.Game import Game
from core.Predictions import Predictions
from core.Display import Display

logging.basicConfig(level=logging.DEBUG,
                    format='%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
                    datefmt='%m-%d %H:%M',
                    filename='/tmp/myapp.log',
                    filemode='w')

console = logging.StreamHandler()
console.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(name)-12s: %(levelname)-8s %(message)s')
console.setFormatter(formatter)
logging.getLogger().addHandler(console)

async def main(loop):
    # Inizializza
    display = Display(loop)
    camera = Camera()
    predictions = Predictions(camera)
    game = Game(predictions, display, camera)

    # Gioca
    display.open()
    await game.play()
    display.close()



if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    logging.debug('===START===')
    loop.run_until_complete(main(loop))
    logging.debug('===STOP===')
